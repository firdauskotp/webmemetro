import discord
import os
from discord.ext import commands
from discord.voice_client import VoiceClient

intents = discord.Intents.default()
intents.members = True

TOKEN = os.getenv('DISCORD_TOKEN')

client = commands.Bot(command_prefix = "wd!", intents = intents)

@client.event
async def on_ready():
    print("WUHU {0.user}".format(client))

@client.event
async def on_member_join(member):
    channel = discord.utils.get(member.guild.channels, name='sit')
    base_greeting = f"Welcome {member.mention} to the official KidoCode Web Department Discord Group. "

    greetings = [
        f"Hope you will be able to make new connections and grow your circle here!"
        f"Don't forget to contribute to our memes >:)"
        f"Hopefully you don't regret it here :')"
    ]

    await member.send("Hello, if you've seen this message, that means that the web department's bot \
    is going to say, congratulations on joining the web department server! I guess? \
    Over here is where we want to grow as web developers, but also find new friends, play games, and most importantly, memes! On a serious note, we also share \
        our socials and most importantly linkedIn / code connections such as GitHub and GitLab. Let's help each other to be a more established web developer whether individually or \
            as a team! All the best and enjoy here. If you run into any trouble, don't hesitate to ask the admins(managers) or moderators(executors). For more info on this bot, use \
                the command wd!help . However, there are some rules you need to adhere here \n \n \
                    \t 1. Be respectful to each other \
                    \t 2. No postings of inappropriate contents \
                    \t 3. Unless approved by mods or admins, no posting sensitive issues such as politics \
                    \t 4. Introduce yourself and share your linkedIn / code repositories if possible, so that we all can grow together ^_^ \
                    \t 5. Do not tag the mods and admins unnecessarily, it will result in a warning and more of it, the banhammer of justice will be reigning on you \
                    \n \n \
                    Pretty basic rules, anyways, enjoy yourself here!")

client.run('TOKEN')